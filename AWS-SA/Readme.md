## Notes for AWS Solutions Architect - Associate

These notes are based on [Stephane Maarek's](https://www.linkedin.com/in/stephanemaarek/) course and these following repositories:

- [aireddy73/SAA-C02](https://github.com/aireddy73/SAA-C02)
- [keenanromain/AWS-SAA-C02-Study-Guide](https://github.com/keenanromain/AWS-SAA-C02-Study-Guide)
- [solvimm/aws-solutions-architect-associate-labs](https://github.com/solvimm/aws-solutions-architect-associate-labs)

### Course Outline

<img src='./Course-Structure.jpg'>

### AWS Fundamentals

<img src='./Foundation.jpg'>

### History and Evolution of AWS

<img src='./History.jpg'>

### EC2 - Elastic Compute Cloud

<img src='./EC2.jpg'>
<img src='./EC2-II.jpg'>
<img src='./EC2-III.jpg'>

### EC2 - Elastic Compute Cloud - Associate Level

<img src='./EC2-SAC.jpg'>
<img src='./EC2-SAC-II.jpg'>

### EC2 Instance Storage

<img src='./EC2-Storage.jpg'>
<img src='./EC2-Storage-II.jpg'>
<img src='./EC2-Storage-III.jpg'>

### Load Balancing

<img src='./LB.jpg'>
<img src='./LB-II.jpg'>

### Auto Scaling & SNI (Server Name Indication)

<img src='./SSL.jpg'>
